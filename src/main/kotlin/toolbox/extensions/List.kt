package toolbox.extensions

/**
 * Swap two elements in a list at positions [i1] and [i2] where [i1] < [i2]
 */
fun <T> List<T>.swap(i1: Int, i2: Int): List<T> {
    require(i1 < i2) { "i1 < i2 must hold" }
    return this.subList(0, i1) +
            this[i2] +
            this.subList(i1 + 1, i2) +
            this[i1] +
            this.subList(i2 + 1, this.size)
}