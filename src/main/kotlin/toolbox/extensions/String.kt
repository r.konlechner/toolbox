package toolbox.extensions

fun String.dropLines(n: Int) = this
    .lines()
    .drop(n)
    .joinToString("\n")

fun String.dropLastLines(n: Int) = this
    .lines()
    .dropLast(n)
    .joinToString("\n")

fun String.indexesOf(char: Char): List<Int> {
    val result = mutableListOf<Int>()
    var index = this.indexOf(char)
    while (index >= 0) {
        result.add(index)
        index = this.indexOf(char, index + 1)
    }
    return result
}

fun String.replaceAt(index: Int, char: Char): String {
    return this.substring(0, index) + char + this.substring(index + 1)
}
