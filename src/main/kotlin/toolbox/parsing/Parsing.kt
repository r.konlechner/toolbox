package toolbox.parsing

class Parsing {
    /**
     * Get the comparison function from a relational operator symbol
     */
    fun <A : Comparable<A>> parseComparisonOperator(operator: String): (A, A) -> Boolean {
        return when (operator) {
            "==" -> { x, y -> x == y }
            "!=" -> { x, y -> x != y }
            ">" -> { x, y -> x > y }
            ">=" -> { x, y -> x >= y }
            "<" -> { x, y -> x < y }
            "<=" -> { x, y -> x <= y }
            else -> error("Unknown operator: $operator")
        }
    }

    /**
     * Get the arithmetic function from an arithmetic operator symbol
     */
    fun parseArithmeticOperator(operator: String): (Int, Int) -> Int {
        return when (operator) {
            "+" -> { x, y -> x + y }
            "-" -> { x, y -> x - y }
            "*" -> { x, y -> x * y }
            "/" -> { x, y -> x / y }
            "%" -> { x, y -> x % y }
            else -> error("Unknown operator: $operator")
        }
    }


}