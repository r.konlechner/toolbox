package toolbox.validation

import com.github.kittinunf.result.Result
import java.lang.IllegalStateException

class ValidationException(
    val errors: List<String>
) : Exception()

fun <T> T.validate() = Validator(Result.success(this))

class Validator<T>(private val input: Result<T, ValidationException>) {

    fun check(errorMsg: String, rule: (T) -> Boolean?): Validator<T> {
        return when (input) {
            is Result.Success -> {
                try {
                    if (rule(input.value) == true) {
                        Validator(input)
                    } else {
                        Validator(failure(errorMsg))
                    }
                } catch (e: IllegalArgumentException) {
                    Validator(failure(errorMsg, e.message))
                } catch (e: IllegalStateException) {
                    Validator(failure(errorMsg, e.message))
                }
            }
            is Result.Failure -> Validator(input)
        }
    }

    fun <U> check(rule: (T) -> Result<U, ValidationException>): Validator<U> {
        return when (input) {
            is Result.Success -> {
                try {
                    Validator(rule(input.value))
                } catch (e: IllegalArgumentException) {
                    Validator(failure(e.message))
                } catch (e: IllegalStateException) {
                    Validator(failure(e.message))
                }
            }
            is Result.Failure -> Validator(input)
        }
    }

    fun <U> checkAndMap(errorMsg: String? = null, transform: (T) -> U): Validator<U> {
        return when (input) {
            is Result.Success -> {
                try {
                    Validator(success(transform(input.value)))
                } catch (e: IllegalArgumentException) {
                    Validator(failure(errorMsg, e.message))
                } catch (e: IllegalStateException) {
                    Validator(failure(errorMsg, e.message))
                }
            }
            is Result.Failure -> Validator(input)
        }
    }

    fun result() = input
}

fun <T> success(result: T): Result.Success<T> = Result.success(result)
fun failure(msg: List<String>): Result.Failure<ValidationException> = Result.error(ValidationException(msg.toList()))
fun failure(vararg msg: String?) = failure(msg.toList().filterNotNull())

fun Result<*, ValidationException>.errorList(): List<String> {
    return when (this) {
        is Result.Success -> emptyList()
        is Result.Failure -> this.error.errors
    }
}
