package toolbox.reports

import java.io.File

class Report {
    fun saveReport(report: String, path: String) {
        val file = File(path)
        file.createNewFile()
        file.writeText(report)
    }

    /**
     * Create Markdown from a Kotlin source file
     */
    fun createMarkdownReport(fileName: String, placeholders: Map<String, Any?> = emptyMap()): String {
        val lines = File(fileName).readLines()

        val code = lines
            .asSequence()
            .filter { !it.isPackageDeclaration() }
            .filter { !it.isImport() }
            .filter { !it.isPrintStatement() }
            .filter { !it.isMainDeclaration() }
            .filter { !it.isMainClosingBracket() }
            .filter { !it.isEmptyLine() }
            .takeWhile { !it.isAppendixDeclaration() }
            .toList()
            .dropLast(1) // do not include Appendix line

        return (code.mapIndexed { index, line ->
            when {
                isHeader(index, code) -> h1(line.removeCommentPrefix())
                line.isJavaDocDelimiter() -> ""
                line.isHeaderDelimiter() -> ""
                line.isComment() -> line.removeCommentPrefix()
                else -> {
                    when {
                        isSingleCodeLine(index, code) -> codeBlockSingle(line, "kotlin")
                        isFirstCodeLine(index, code) -> beginCodeBlock(line, "kotlin")
                        isLastCodeLine(index, code) -> endCodeBlock(line)
                        else -> codeBlock(line)
                    }
                }
            }
        } + listOf("\n***\n*This document was generated*"))
            .joinToString("\n")
            .replaceAll(placeholders).removePrefix("\n\n")
    }

    private fun String.isPackageDeclaration() = this.trim().startsWith("package")
    private fun String.isImport() = this.trim().startsWith("import")
    private fun String.isMainDeclaration() = this.startsWith("fun main")
    private fun String.isMainClosingBracket() = this.startsWith("}")
    private fun String.isEmptyLine() = this.trim().isEmpty()
    private fun String.isPrintStatement() = this.trim().startsWith("print")
    private fun String.isAppendixDeclaration() = this.trim().startsWith("* Appendix")
    private fun String.isJavaDocDelimiter() = this.trim().let { it.startsWith("/**") || it.startsWith("*/") }
    private fun String.isComment() = this.trim().let { it.startsWith("/") || it.startsWith("*") }
    private fun String.isHeaderDelimiter() = this.trim().startsWith("* ***")

    private fun isHeader(index: Int, code: List<String>): Boolean {
        return (index > 0 && code[index - 1].trim().startsWith("* ***"))
                && (index < (code.size - 1) && code[index + 1].trim().startsWith("* ***"))
    }

    private fun isSingleCodeLine(index: Int, code: List<String>): Boolean {
        return (index > 0 && code[index - 1].isComment())
                && (index < (code.size - 1) && code[index + 1].isComment())
    }

    private fun isFirstCodeLine(index: Int, code: List<String>): Boolean {
        return index > 0 && code[index - 1].isComment()
    }

    private fun isLastCodeLine(index: Int, code: List<String>): Boolean {
        return index == code.size - 1 || index < (code.size - 1) && code[index + 1].isComment()
    }

    private fun String.removeCommentPrefix() = this.trim()
        .removePrefix("*")
        .removePrefix("//")
        .trim()

    private fun String.replaceAll(placeholders: Map<String, Any?>): String {
        var result = this
        for ((key, value) in placeholders) {
            result = result.replace("{{$key}}", value?.toString() ?: "")
        }
        return result
    }

    private fun h1(line: String) = "# $line"
    private fun beginCodeBlock(line: String, language: String) = "```$language\n${line.removePrefix("    ")}"
    private fun codeBlockSingle(line: String, language: String) = "```$language\n${line.removePrefix("    ")}\n```"
    private fun codeBlock(line: String) = line.removePrefix("    ")
    private fun endCodeBlock(line: String) = "${line.removePrefix("    ")}\n```"
}