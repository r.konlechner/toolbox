package toolbox.resources

import java.net.URL
import java.nio.charset.Charset

fun resourceURL(path: String): URL = object {}::class.java.getResource(path)

fun readTextFromResource(path: String, charset: Charset = Charsets.UTF_8): String =
    resourceURL(path).readText(charset)
