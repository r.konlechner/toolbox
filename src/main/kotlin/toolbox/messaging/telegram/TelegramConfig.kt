package toolbox.messaging.telegram

data class TelegramConfig(
    val url: String,
    val token: String,
    val chatId: String
)