package toolbox.messaging.telegram

import com.github.kittinunf.fuel.httpPost

class Telegram(
    private val telegramConfig: TelegramConfig
) {
    fun postMessage(message: String) {
        telegramConfig.run {
            "$url/bot$token/sendMessage"
                .httpPost(
                    listOf(
                        "chat_id" to chatId,
                        "text" to message
                    ),
                )
                .response()
        }
    }
}