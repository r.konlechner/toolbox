package toolbox.messaging.slack

import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.kittinunf.fuel.httpPost

class Slack(private val url: String) {
    fun postMessage(message: String) {
        url.httpPost()
            .jsonBody("""{"text": "$message"}""")
            .response()
    }
}