package toolbox.formats.json.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.time.ZonedDateTime

class ZonedDateTimeAdapter : TypeAdapter<ZonedDateTime>() {

    override fun write(out: JsonWriter, value: ZonedDateTime) {
        out.value(value.toString())
    }

    override fun read(input: JsonReader): ZonedDateTime {
       return ZonedDateTime.parse(input.nextString())
    }
}
