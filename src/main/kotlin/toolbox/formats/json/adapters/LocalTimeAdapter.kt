package toolbox.formats.json.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.time.LocalTime

class LocalTimeAdapter : TypeAdapter<LocalTime>() {

    override fun write(out: JsonWriter, value: LocalTime) {
        out.value(value.toString())
    }

    override fun read(input: JsonReader): LocalTime {
       return LocalTime.parse(input.nextString())
    }
}
