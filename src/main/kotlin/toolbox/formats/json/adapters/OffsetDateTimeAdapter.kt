package toolbox.formats.json.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import java.time.OffsetDateTime

class OffsetDateTimeAdapter : TypeAdapter<OffsetDateTime>() {

    override fun write(out: JsonWriter, value: OffsetDateTime) {
        out.value(value.toString())
    }

    override fun read(input: JsonReader): OffsetDateTime {
       return OffsetDateTime.parse(input.nextString())
    }
}
