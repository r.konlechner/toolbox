package toolbox.formats.json

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import toolbox.formats.json.adapters.*
import java.time.*

val gson: Gson = GsonBuilder()
        .registerTypeAdapter(LocalDate::class.java, LocalDateAdapter())
        .registerTypeAdapter(LocalTime::class.java, LocalTimeAdapter())
        .registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeAdapter())
        .registerTypeAdapter(OffsetDateTime::class.java, OffsetDateTimeAdapter())
        .create()

fun Any.toJson(): String = gson.toJson(this)

inline fun <reified T : Any> String.fromJson(): T {
    return gson.fromJson(
        this,
        when(T::class) {
            List::class -> object : TypeToken<T>() {}.type
            else -> T::class.java
        }
    )
}
