package toolbox.data

import tech.tablesaw.api.IntColumn
import tech.tablesaw.api.Table
import tech.tablesaw.io.csv.CsvReadOptions
import toolbox.extensions.*

/**
 * Returns a copy of the table with a new index column at the beginning
 */
fun Table.withIndexColumn(): Table {
    return this.insertColumn(0, IntColumn.create("Index", Array(this.count()) { i -> i }))
}

/**
 * A small modification of `printAll()` to align with the Markdown format for tables
 */
fun Table.printMarkdown(rowLimit: Int = 20): String {
    val all = this.print(rowLimit).lines()
    val titleLine = all.first()
    val firstTableLine = all.component2()
    var separatorLine = all.component3()
    val pipePositions = firstTableLine.indexesOf('|')
    pipePositions.forEach { separatorLine = separatorLine.replaceAt(it, '|') }
    return (
        listOf("**${titleLine.trim()}**\n") +
            listOf("|$firstTableLine") +
            listOf("|$separatorLine") +
            all.drop(3).map { "|$it" })
        .joinToString("\n")
}

/**
 * A small modification of `printAll()` to align with the Markdown format for tables
 */
fun Table.printAllMarkdown(): String {
    val all = this.printAll().lines()
    val titleLine = all.first()
    val firstTableLine = all.component2()
    var separatorLine = all.component3()
    val pipePositions = firstTableLine.indexesOf('|')
    pipePositions.forEach { separatorLine = separatorLine.replaceAt(it, '|') }
    return (
        listOf("**${titleLine.trim()}**\n") +
            listOf("|$firstTableLine") +
            listOf("|$separatorLine") +
            all.drop(3).map { "|$it" })
        .joinToString("\n")
}

/**
 * Create a table from CSV
 * @param header is true if first line is header line
 * @throws IndexOutOfBoundsException if values contain unescaped [separator] chars
 */
fun String.csvToTable(separator: Char = ',', header: Boolean = false): Table {
    val builder = CsvReadOptions.builderFromString(this)
        .separator(separator)
        .header(header)
    val options = builder.build()
    return Table.read().usingOptions(options)
}
