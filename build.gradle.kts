import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.8.0"
    `java-library`
    `maven-publish`
}

group = "com.konlechner.rafael"
version = "0.1.0-SNAPSHOT"

repositories {
    mavenCentral()
    gradlePluginPortal()
}

dependencies {

    implementation("com.github.kittinunf.fuel:fuel:2.3.1")
    implementation("tech.tablesaw:tablesaw-core:0.43.1")
    implementation("com.google.code.gson:gson:2.10.1")

    testImplementation(platform("org.junit:junit-bom:5.9.0"))
    testImplementation("org.junit.jupiter:junit-jupiter:5.9.2")
}

java {
    targetCompatibility = JavaVersion.VERSION_17
    withSourcesJar()
}

tasks {
    test {
        useJUnitPlatform()
    }
    jar {
        manifest {
            attributes(
                mapOf(
                    "Implementation-Title" to project.name,
                    "Implementation-Version" to project.version
                )
            )
        }
    }
    withType<KotlinCompile>().configureEach {
        kotlinOptions {
            jvmTarget = "17"
        }
    }
}

val gitlabPrivateToken: String by project
val gitlabUrl = "https://gitlab.com/api/v4/projects/22875621/packages/maven"

publishing {
    repositories {
        maven {
            name = "Gitlab"
            url = uri(gitlabUrl)
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
    publications {
        create<MavenPublication>("toolbox") {
            from(components["java"])
        }
    }
}
